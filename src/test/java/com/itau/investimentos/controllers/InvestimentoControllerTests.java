package com.itau.investimentos.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itau.investimentos.enums.Risco;
import com.itau.investimentos.models.Investimento;
import com.itau.investimentos.models.Simulacao;
import com.itau.investimentos.services.InvestimentoService;
import com.itau.investimentos.services.SimulacaoService;
import org.hamcrest.CoreMatchers;
import org.hibernate.ObjectNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

@WebMvcTest(InvestimentoController.class)
public class InvestimentoControllerTests {

    @MockBean
    InvestimentoService investimentoService;

    @MockBean
    SimulacaoService simulacaoService;

    @Autowired
    private MockMvc mockMvc;

    ObjectMapper mapper = new ObjectMapper();
    Investimento investimento;
    Simulacao simulacao;

    @BeforeEach
    public void inicializar(){
        investimento = new Investimento();
        investimento.setId(1);
        investimento.setNome("Tesouro Direto");
        investimento.setRisco(Risco.BAIXO);
        investimento.setPorcentagemLucro(1.10);

        simulacao = new Simulacao();
        simulacao.setResultado(0);
        simulacao.setValorInvestimento(100);
        simulacao.setQuantidadeMeses(1);
        simulacao.setIdInvestimento(2);
    }

    @Test
    public void testarBuscarTodosInvestimentosSucesso() throws Exception {
        Mockito.when(investimentoService.buscarTodosInvestimentos()).thenReturn(Arrays.asList(investimento));

        mockMvc.perform(MockMvcRequestBuilders.get("/investimentos")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0]").exists());
    }

    @Test
    public void testarBuscarTodosInvestimentosVazio() throws Exception {
        Mockito.when(investimentoService.buscarTodosInvestimentos()).thenReturn(new ArrayList<Investimento>());

        mockMvc.perform(MockMvcRequestBuilders.get("/investimentos")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isEmpty());
    }

    @Test
    public void testarBuscarInvestimentoSucesso() throws Exception {
        Mockito.when(investimentoService.buscarPorId(Mockito.anyInt())).thenReturn(Optional.of(investimento));

        int id = 1;

        mockMvc.perform(MockMvcRequestBuilders.get("/investimentos/" +id)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id"
                        , CoreMatchers.equalTo(1)));
    }

    @Test
    public void testarBuscarInvestimentoInexistente() throws Exception {
        Mockito.when(investimentoService.buscarPorId(Mockito.anyInt())).thenReturn(Optional.empty());

        int id = 3;

        mockMvc.perform(MockMvcRequestBuilders.get("/investimentos/" +id)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarCriarInvestimentoSucesso() throws Exception {
        Mockito.when(investimentoService.criarInvestimento(Mockito.any(Investimento.class)))
                .thenReturn(investimento);
        String json = mapper.writeValueAsString(investimento);
        mockMvc.perform(MockMvcRequestBuilders.post("/investimentos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id"
                        , CoreMatchers.equalTo(1)));
    }

    @Test
    public void testarCriarInvestimentoNomeInvalido() throws Exception {
        Mockito.when(investimentoService.criarInvestimento(Mockito.any(Investimento.class))).thenReturn(investimento);
        investimento.setNome(null);
        String json = mapper.writeValueAsString(investimento);

        mockMvc.perform(MockMvcRequestBuilders.post("/investimentos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarCriarInvestimentoPorcentualInvalido() throws Exception {
        Mockito.when(investimentoService.criarInvestimento(Mockito.any(Investimento.class))).thenReturn(investimento);
        investimento.setPorcentagemLucro(0);
        String json = mapper.writeValueAsString(investimento);

        mockMvc.perform(MockMvcRequestBuilders.post("/investimentos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarCriarInvestimentoPorcentualLimite() throws Exception {
        Mockito.when(investimentoService.criarInvestimento(Mockito.any(Investimento.class))).thenReturn(investimento);
        investimento.setPorcentagemLucro(0.1);
        String json = mapper.writeValueAsString(investimento);

        mockMvc.perform(MockMvcRequestBuilders.post("/investimentos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void testarAtualizarInvestimento() throws Exception {
        Investimento investimentoAtualizado = investimento;
        investimentoAtualizado.setNome("Nova Aplicação");

        Mockito.when(investimentoService.atualizarInvestimento(Mockito.any(Investimento.class))).thenReturn(investimentoAtualizado);
        String json = mapper.writeValueAsString(investimentoAtualizado);
        int id = 1;

        mockMvc.perform(MockMvcRequestBuilders.put("/investimentos/"+id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome"
                        , CoreMatchers.equalTo(investimentoAtualizado.getNome())));
    }

    @Test
    public void testarAtualizarInvestimentoInexistente() throws Exception {
        Investimento investimentoAtualizado = investimento;
        investimentoAtualizado.setNome("Nova Aplicação");
        Mockito.when(investimentoService.atualizarInvestimento(Mockito.any(Investimento.class)))
                .thenThrow(new ObjectNotFoundException(Investimento.class, "Não foi encontrado o investimento selecionado"));

        String json = mapper.writeValueAsString(investimentoAtualizado);
        int id = 1;

        mockMvc.perform(MockMvcRequestBuilders.put("/investimentos/"+id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarRemoverInvestimentoInexistente() throws Exception {
        Mockito.when(investimentoService.buscarPorId(Mockito.anyInt())).thenReturn(Optional.empty());

        int id = 3;
        String json = mapper.writeValueAsString(investimento);

        mockMvc.perform(MockMvcRequestBuilders.delete("/investimentos/" +id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarRemoverInvestimentoSucesso() throws Exception {
        Mockito.when(investimentoService.buscarPorId(Mockito.anyInt())).thenReturn(Optional.of(investimento));

        int id = 1;
        String json = mapper.writeValueAsString(investimento);

        mockMvc.perform(MockMvcRequestBuilders.delete("/investimentos/" +id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    public void testarCalcularSimulacaoSucesso() throws Exception {
        Mockito.when(investimentoService.buscarPorId(Mockito.anyInt())).thenReturn(Optional.of(investimento));
        String json = mapper.writeValueAsString(simulacao);

        mockMvc.perform(MockMvcRequestBuilders.put("/investimentos/simulacao/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.resultado"
                        , CoreMatchers.equalTo(simulacao.getResultado())));
    }
    @Test
    public void testarCalcularSimulacaoQtdeMesesInvalido() throws Exception {
        Mockito.when(investimentoService.buscarPorId(Mockito.anyInt())).thenReturn(Optional.of(investimento));
        simulacao.setQuantidadeMeses(0);
        String json = mapper.writeValueAsString(simulacao);

        mockMvc.perform(MockMvcRequestBuilders.put("/investimentos/simulacao/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarCalcularSimulacaoValorInvestidoInvalido() throws Exception {
        Mockito.when(investimentoService.buscarPorId(Mockito.anyInt())).thenReturn(Optional.of(investimento));
        simulacao.setValorInvestimento(10);
        String json = mapper.writeValueAsString(simulacao);

        mockMvc.perform(MockMvcRequestBuilders.put("/investimentos/simulacao/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarCalcularSimulacaoInvestimentoInexisitente() throws Exception {
        Mockito.when(investimentoService.buscarPorId(Mockito.anyInt())).thenReturn(Optional.empty());
        String json = mapper.writeValueAsString(simulacao);

        mockMvc.perform(MockMvcRequestBuilders.put("/investimentos/simulacao/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.resultado"
                        , CoreMatchers.equalTo(simulacao.getResultado())));
    }
}