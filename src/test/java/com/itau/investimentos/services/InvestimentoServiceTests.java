package com.itau.investimentos.services;

import com.itau.investimentos.enums.Risco;
import com.itau.investimentos.models.Investimento;
import com.itau.investimentos.repositories.InvestimentoRepository;
import org.hibernate.ObjectNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.Optional;

@SpringBootTest
public class InvestimentoServiceTests {
    @MockBean
    InvestimentoRepository investimentoRepository;

    @Autowired
    InvestimentoService investimentoService;

    Investimento investimento;

    @BeforeEach
    public void inicializarTestes() {
        investimento = new Investimento();
        investimento.setId(1);
        investimento.setNome("Teste Tesouro");
        investimento.setPorcentagemLucro(1.10);
        investimento.setRisco(Risco.MEDIO);
    }

    @Test
    public void testarBuscarPorIdSucesso(){
        Mockito.when(investimentoRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(investimento));
        int id = 1;
        Optional investimentoOptional = investimentoService.buscarPorId(id);
        Assertions.assertEquals(investimento, investimentoOptional.get());
    }

    @Test
    public void testarBuscarPorIdInexistente(){
        Mockito.when(investimentoRepository.findById(Mockito.anyInt())).thenReturn(Optional.empty());
        int id = 1;
        Optional investimentoOptional = investimentoService.buscarPorId(id);
        Assertions.assertFalse(investimentoOptional.isPresent());
    }

    @Test
    public void testarCriarInvestimentoSucesso(){
        Mockito.when(investimentoRepository.save(Mockito.any(Investimento.class))).thenReturn(investimento);
        Investimento investimentoObjeto = investimentoService.criarInvestimento(investimento);
        Assertions.assertEquals(investimento, investimentoObjeto);
    }

    @Test
    public void testarBuscarTodosInvestimentosSucesso(){
        Iterable<Investimento> investimentosIterable = Arrays.asList(investimento);
        Mockito.when(investimentoRepository.findAll()).thenReturn(investimentosIterable);
        Iterable<Investimento> iterableResultado = investimentoService.buscarTodosInvestimentos();
        Assertions.assertEquals(investimentosIterable, iterableResultado);
    }

    @Test
    public void testarBuscarTodosInvestimentosVazio(){
        Iterable<Investimento> investimentosIterable = Arrays.asList(new Investimento());
        Mockito.when(investimentoRepository.findAll()).thenReturn(investimentosIterable);
        Iterable<Investimento> iterableResultado = investimentoService.buscarTodosInvestimentos();
        Assertions.assertEquals(investimentosIterable, iterableResultado);
    }

    @Test
    public void testarAtualizarInvestimentoSucesso(){
        Mockito.when(investimentoRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(investimento));
        Mockito.when(investimentoRepository.save(Mockito.any(Investimento.class))).thenReturn(investimento);
        Investimento investimentoObjeto = investimentoService.atualizarInvestimento(investimento);
        Assertions.assertEquals(investimento, investimentoObjeto);
    }

    @Test
    public void testarAtualizarInvestimentoInexistente(){
        Mockito.when(investimentoRepository.findById(Mockito.anyInt())).thenReturn(Optional.empty());
        Mockito.when(investimentoRepository.save(Mockito.any(Investimento.class))).thenReturn(null);
        Assertions.assertThrows(ObjectNotFoundException.class, ()->{investimentoService.atualizarInvestimento(investimento);});
    }

    @Test
    public void testarAtualizarInvestimentoNomeInvalido(){
        Investimento investimentoAtualizado = investimento;
        investimentoAtualizado.setNome(null);
        Mockito.when(investimentoRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(investimento));
        Mockito.when(investimentoRepository.save(Mockito.any(Investimento.class))).thenReturn(investimentoAtualizado);
        Investimento investimentoObjeto = investimentoService.atualizarInvestimento(investimentoAtualizado);
        Assertions.assertEquals(investimentoObjeto, investimentoAtualizado);
    }

    @Test
    public void testarApagarInvestimentoSucesso(){
        investimentoService.apagarInvestimento(investimento);
        Mockito.verify(investimentoRepository, Mockito.times(1))
                .delete(Mockito.any(Investimento.class));
    }
}
