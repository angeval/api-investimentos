package com.itau.investimentos.services;

import com.itau.investimentos.enums.Risco;
import com.itau.investimentos.models.Investimento;
import com.itau.investimentos.models.Simulacao;
import com.itau.investimentos.repositories.InvestimentoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

@SpringBootTest
public class SimulacaoServiceTests {
    @MockBean
    InvestimentoRepository investimentoRepository;

    @Autowired
    InvestimentoService investimentoService;

    @Autowired
    SimulacaoService simulacaoService;

    Investimento investimento;
    Simulacao simulacao;

    @BeforeEach
    public void inicializarTestes() {
        investimento = new Investimento();
        investimento.setId(1);
        investimento.setNome("Teste Tesouro");
        investimento.setPorcentagemLucro(10.0);
        investimento.setRisco(Risco.MEDIO);

        simulacao = new Simulacao();
        simulacao.setIdInvestimento(investimento.getId());
        simulacao.setQuantidadeMeses(1);
        simulacao.setValorInvestimento(100);
    }

    @Test
    public void testarCalcularSimulacaoSimples(){
        Mockito.when(investimentoRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(investimento));
        Double resultado = new Double(110);
        Assertions.assertEquals(resultado, simulacaoService.calcularSimulacao(simulacao));
    }

    @Test
    public void testarCalcularSimulacaoInvestimentoInexistente(){
        Mockito.when(investimentoRepository.findById(Mockito.anyInt())).thenReturn(Optional.empty());
        Assertions.assertEquals(new Double(0), simulacaoService.calcularSimulacao(simulacao));
    }
}
