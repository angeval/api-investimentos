package com.itau.investimentos.services;

import com.itau.investimentos.models.Investimento;
import com.itau.investimentos.repositories.InvestimentoRepository;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class InvestimentoService {
        @Autowired
        private InvestimentoRepository investimentoRepository;

        public Optional<Investimento> buscarPorId(int id){
            Optional<Investimento> investimentoOptional = investimentoRepository.findById(id);
            return investimentoOptional;
        }

        public Investimento criarInvestimento(Investimento investimento){
            Investimento investimentoObjeto = investimentoRepository.save(investimento);
            return investimentoObjeto;
        }

        public Iterable<Investimento> buscarTodosInvestimentos(){
            Iterable<Investimento> investimento = investimentoRepository.findAll();
            return investimento;
        }

        public Investimento atualizarInvestimento(Investimento investimento){
            Optional<Investimento> investimentoOptional = buscarPorId(investimento.getId());
            if (investimentoOptional.isPresent()){
                Investimento investimentoData = investimentoOptional.get();
                Investimento investimentoObjeto = investimentoRepository.save(investimento);
                return investimentoObjeto;
            }
            else {
                throw new ObjectNotFoundException(Investimento.class,"Não foi encontrado o investimento selecionado");
            }
        }

        public void apagarInvestimento(Investimento investimento){
            investimentoRepository.delete(investimento);
        }

}
