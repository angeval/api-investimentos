package com.itau.investimentos.services;

import com.itau.investimentos.models.Investimento;
import com.itau.investimentos.models.Simulacao;
import com.itau.investimentos.repositories.InvestimentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Service
public class SimulacaoService {

    @Autowired
    private InvestimentoRepository investimentoRepository;

    public Double calcularSimulacao(Simulacao simulacao){
        Optional<Investimento> investimentoOptional = investimentoRepository.findById(simulacao.getIdInvestimento());
        if (investimentoOptional.isPresent()){
             simulacao.setResultado(Math.round(
                     simulacao.getValorInvestimento() *
                     Math.pow(
                     (1 + (investimentoOptional.get().getPorcentagemLucro()/100))
                     ,        simulacao.getQuantidadeMeses())));}
        return simulacao.getResultado();
    }
}
