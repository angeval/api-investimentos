package com.itau.investimentos.models;

import com.itau.investimentos.enums.Risco;
import org.springframework.beans.factory.annotation.Value;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.*;

@Entity
public class Investimento {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Size(min=0, max=150, message = "Nome do investimento inválido")
    @NotNull(message="Informar o nome do investimento")
    private String nome;
    @NotNull(message="Informar o grau de risco do investimento")
    private Risco risco;
    @Min(0)
    @DecimalMin("0.1")
    //@Digits(integer=0, fraction=1)
    private double porcentagemLucro;

    public Investimento() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Risco getRisco() {
        return risco;
    }

    public void setRisco(Risco risco) {
        this.risco = risco;
    }

    public double getPorcentagemLucro() {
        return porcentagemLucro;
    }

    public void setPorcentagemLucro(double porcentagemLucro) {
        this.porcentagemLucro = porcentagemLucro;
    }
}
