package com.itau.investimentos.controllers;

import com.itau.investimentos.models.Investimento;
import com.itau.investimentos.models.Simulacao;
import com.itau.investimentos.services.InvestimentoService;
import com.itau.investimentos.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import javax.validation.Valid;
import java.util.Optional;

@RequestMapping("/investimentos")
@RestController
public class InvestimentoController {
    @Autowired
    //instanciar essa classe para todas as classes
    private InvestimentoService investimentoService;

    @Autowired
    private SimulacaoService simulacaoService;

    @GetMapping
    //Buscar todos da lista
    public Iterable<Investimento> buscarTodosInvestimentos() {
        return investimentoService.buscarTodosInvestimentos();
    }

    @GetMapping("/{id}")
    public Optional<Investimento> buscarInvestimento(@PathVariable int id) {
        //PathVariable vai indicar que ele vai receber essa variável da URL
        Optional<Investimento> investimentoOptional = investimentoService.buscarPorId(id);
        if (investimentoOptional.isPresent()) {
            return investimentoOptional;
        } else{
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping
    //Insere na base
    public ResponseEntity<Investimento> criarInvestimento(@RequestBody @Valid Investimento investimento) {
        Investimento investimentoObjeto = investimentoService.criarInvestimento(investimento);
        return ResponseEntity.status(201).body(investimentoObjeto);
    }

    @PutMapping("/simulacao")
    public ResponseEntity<Simulacao> simularInvestimento(@RequestBody @Valid Simulacao simulacao) {
        simulacao.setResultado(simulacaoService.calcularSimulacao(simulacao));
        return ResponseEntity.status(200).body(simulacao);
    }

    @PutMapping("/{id}")
    //Atualiza registro na base
    public ResponseEntity<Investimento> atualizarInvestimento(@PathVariable Integer id, @RequestBody Investimento investimento) {
        investimento.setId(id);
        try {
            Investimento investimentoObjeto = investimentoService.atualizarInvestimento(investimento);
            return ResponseEntity.status(200).body(investimentoObjeto);
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getLocalizedMessage());
        }
    }

    @DeleteMapping("/{id}")
    //Deleta registro da base
    public ResponseEntity<Investimento> apagarInvestimento(@PathVariable Integer id) {
        Optional<Investimento> investimentoOptional = investimentoService.buscarPorId(id);
        if (investimentoOptional.isPresent()) {
            investimentoService.apagarInvestimento(investimentoOptional.get());
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.status(204).body(investimentoOptional.get());
    }
}