package com.itau.investimentos.enums;

public enum Risco {
    ALTO,
    MEDIO,
    BAIXO;

}
